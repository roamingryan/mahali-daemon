from flask import render_template, request, session, flash, url_for, redirect, send_from_directory
from flask import make_response

from webif import app
from sizeof import sizeof_fmt
import json
import os
import subprocess

@app.route("/test")
def testview():
    return str(app.APPCONFIG)

@app.route("/")
def mahali_main():
    return render_template("mahali_main.html",config=app.APPCONFIG)

@app.route("/science")
def mahali_science():
    """ Human readable view of science logs """

    path = os.path.join(app.APPCONFIG['LOG_PATH'],'science')
    filenames = next(os.walk(path))[2]
    filenames.sort()

    table = []
    for f in filenames:
        size = os.path.getsize(os.path.join(path,f))
        table.append((f,sizeof_fmt(size)))

    return render_template("mahali_science.html",data=table)

@app.route("/admin")
def mahali_admin():
    """ Human readable view of admin logs """

    path = os.path.join(app.APPCONFIG['LOG_PATH'],'admin')
    filenames = next(os.walk(path))[2]
    filenames.sort()

    table = []
    for f in filenames:
        size = os.path.getsize(os.path.join(path,f))
        table.append((f,sizeof_fmt(size)))

    return render_template("mahali_admin.html",data=table)

@app.route('/logs/<path:filename>')
def serve_logs(filename):
    """ Serve log files """

    return send_from_directory(app.APPCONFIG['LOG_PATH'], filename)

@app.route('/rinex/<filename>')
def conv_rinex(filename):
    """ Invoke tecq to convert to RINEX """

    # Get path to teqc...this is shameful, but I'm going to do it anyway
    teqc_exec = os.path.abspath(os.path.join(app.APPCONFIG['LOG_PATH'],os.pardir,'teqc','teqc'))
    
    # Construct path of science logfile
    p = os.path.join(app.APPCONFIG['LOG_PATH'],'science',filename)

    teqc_params = [teqc_exec,app.APPCONFIG['TEQC_OPTS'],p]
    print "Running teqc",teqc_params
    rinex = subprocess.check_output(teqc_params)

    # Download RINEX output as a file
    response = make_response(rinex)
    # Set the right header for the response
    # to be downloaded, instead of just printed on the browser
    # http://runnable.com/UiIdhKohv5JQAAB6/how-to-download-a-file-generated-on-the-fly-in-flask-for-python
    response.headers["Content-Disposition"] = "attachment; filename=%s.rinex"%filename
    return response
    
#@app.route("/")
#def main_page():
#    return redirect(url_for('telemetry_list'))

# @app.route("/stats/deframer")
# def deframer_stats():
#     return json.dumps(app.transport.protocol.pkt_deframer.stats,
#                       sort_keys=True, indent=4, separators=(',', ': '))

# @app.route("/stats/framer")
# def framer_stats():
#     return json.dumps(app.transport.protocol.pkt_framer.stats)

# @app.route("/link_stats")
# def link_stats():
#     fs = app.transport.protocol.pkt_framer.stats
#     dfs = app.transport.protocol.pkt_deframer.stats
#     return render_template("link_stats.html", fs=fs, dfs=dfs,title="Link Stats")

# @app.route("/navbar_live")
# def navbar_live():
#     fs = app.transport.protocol.pkt_framer.stats
#     dfs = app.transport.protocol.pkt_deframer.stats
#     return render_template("navbar_live.html", fs=fs, dfs=dfs)
    
# @app.route('/tlmdb')
# def dump_tlm_db():
#     return json.dumps(app.tlm_db)

# @app.route('/tlm')
# def telemetry_list():

#     # Only pass the single most recent telemetry packets of each type
#     recentdb = {}
#     for pkttype in app.tlm_db:
#         recentdb[pkttype] = app.tlm_db[pkttype][-1].copy()
        
#     return render_template("tlm_list.html",db=recentdb,title="TLM Summary")

# @app.route('/tlm/<name>/')
# def telemetry_packet_live(name = None):
#     if name not in app.tlm_db:
#         return "Unknown packet type: %s"%name
#     return render_template("tlm_live.html",name=name, pkt=app.tlm_db[name][-1],title="%s table"%name)
    
# @app.route('/tlm/<name>/data')
# def telemetry_packet_live_data(name = None):
#     if name not in app.tlm_db:
#         return "Unknown packet type: %s"%name
#     return render_template("tlm_live_data.html",name=name, pkt=app.tlm_db[name][-1])

# @app.route('/tlm/<name>/recent/<num>')
# def telemetry_packet_recent(name = None, num=10):
#     numint = int(num)
#     if name not in app.tlm_db:
#         return "Unknown packet type: %s"%name
#     return render_template("tlm_recent.html",name=name, pktlist=app.tlm_db[name][-numint:],title="Recent %s packets"%name)

    
# @app.route('/cmd')
# def command_list():

#     builder = app.transport.protocol.pkt_builder
    
#     return render_template("cmd_list.html",cmds=builder.schema.keys(),title="CMD list" )
    
    
# @app.route('/cmd/<name>', methods=['GET','POST'])
# def commmand_form(name = None):
#     # If this is an HTTP set, then return the packet schema so that the form can be rendered
#     if request.method == 'GET':
#         schema = app.transport.protocol.pkt_builder.schema[name]
#         return render_template("cmd_form.html",schema=schema, title=name)

#     # If this is a post, then deal with the incoming form data
#     if request.method == 'POST':
#         schema = app.transport.protocol.pkt_builder.schema[name]
#         schema['values']={}

#         # Parse all the fields
#         for f in schema['fields']:
#             # Skip padding fields
#             if f['name']=='__padding':
#                 continue
        
#             # If this isn't an array field, then we just get the value
#             if 'array' not in f:
#                 schema['values'][f['name']] = request.form[f['name']]
#                 continue
#             else:
#                 # Values of arrays are POSTed in the format of "NAME,element#"
#                 # This formatting is defined in the HTML template (cmd_form.html)
#                 v_list = []
#                 for elem in range(f['array']):
#                     field_name = "%s,%i"%(f['name'],elem)
#                     v_list.append( request.form[field_name] )
#                 schema['values'][f['name']] = v_list
      
#         # Validate command fields
#         pktb = app.transport.protocol.pkt_builder
#         cmd_schema = pktb.validate_packet(schema)

#         # Transmit the packet
#         app.transport.protocol.transmitPacket(cmd_schema)

#         msg = "Command sent: " + pktb.pretty_print_packet(schema)
#         flash(msg)
#         return redirect(url_for('command_list'))

