####################################################################
# Binary Packet Builder
#
# author: Ryan Kingsbury
#
# This module builds binary packets from Python structures.  The
# Python structures are essentially the same as the JSON packet
# definition, however, they have an added 'values' key that contains
# the value that should be substituted for each of the fields.
#
# TODO: Auto-increment packet sequence ID
#
####################################################################
import struct
import glob
import os
import json

# Get data type mappings
import mapping
DATA_TYPE_FMT = mapping.get_mappings()

def convert(input):
    """ Strip specified object of unicode strings. """
    if isinstance(input, dict):
        return {convert(key): convert(value) for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [convert(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

class Builder:
    
    def __init__(self,verbose=True,schema_path="packet_defs"):

        # Verbose option for debugging
        self._verbose = verbose

        # Schema dictionary, indexed by packet ID
        self.schema = {}

        # Load all schema files in directory
        for fname in glob.iglob(os.path.join(schema_path,'cmd*.json')):
            try: 
                self.load_schema(fname)
            except:
                print "\n\n*****************************************************"
                print "Error parsing packet definition file: %s"%fname
                print "*****************************************************"
                raise

    def load_schema(self,fname):
        """ Loads schema from specified filename and adds it to the
        schema dictionary. """

        with open(fname,'r') as f:
            if self._verbose: print "loading", fname
            # Load JSON file from disk and strip unicode formatting
            j = convert(json.load(f))
            self.schema[j['name']] = j

    def validate_packet(self, pkt):
        """ Validates packet's field values

        This function parses the values specified for each field and
        attempts to convert them into the correct data type for
        transmission.

        Limitations: does not perform range checking

        Raises: ValueError or TypeError
        
        Returns packet data structure with formatted fields."""

        for f in pkt['fields']:

            # Skip padding fields
            if f['name']=='__padding':
                continue

            # integer
            if "int" in f['type']:
                conv_function = int

            # float/double
            elif f['type'] in ['double','float']:
                conv_function = float

            # unknown type
            else:
                raise TypeError

            # If this isn't an array, then just perform the conversion
            if 'array' not in f:
                # Perform the conversion
                pkt['values'][f['name']] = conv_function(pkt['values'][f['name']])

            else:
                # It's an array so we need to convert all of its elements                
                try:
                    array_len = int(f['array'])
                except ValueError:
                    print "Array parameter for field %s is not an integer."
                    raise ValueError

                v_list = []
                for i in range(array_len):
                    v_list.append( conv_function(pkt['values'][f['name']][i]) )
                pkt['values'][f['name']] = v_list

        return pkt
        
    def build_packet(self, pkt):
        """ Builds the packet payload based on the specified Python object.

        Returns: Byte array containing payload.
        """

        if self._verbose: print "Building %s"%pkt['name'],
        
        # Temporary variable where we will construct the packet payload
        buffer = ""

        # Short packets don't have a payload, just return the object with an empty string
        if pkt['format'] == 'short':
            pkt["payloadraw"]=""
            return pkt

        # Process all of the fields, if the aren't found, make them
        # zero
        for f in pkt['fields']:

            #if self._verbose: print "Packing field %s (%s)"%(f['name'],f['type'])
                        
            # Look up the appropriate format description string
            fmt_string = DATA_TYPE_FMT[f['type']]

            # If it is padding, use the default value if specified
            if f['name']=='__padding':
                
                try:
                    val = struct.pack(fmt_string,f['default'])
                except KeyError:
                    # If a default isn't provided, assume zero
                    val = struct.pack(fmt_string,0)
                    
                buffer += val
                continue

            
            # See if this field is an array
            array_len=1
            if 'array' in f:
                try:
                    array_len = int(f['array'])
                    fmt_string = fmt_string*array_len
                except ValueError:
                    print "Array parameter for field %s is not an integer."

            #  We add the '<' to specify little-endian
            fmt_string = "<" + fmt_string

            #if self._verbose: print " Format string after array formatting:",fmt_string

            # Extract the field's value(s)
            try:
                # Also make it a list so that we can pass it in as
                # variable size arguemnt to pack()
                val = pkt['values'][f['name']]
                
                if type(val) is not list:
                    val = [val]

            except KeyError:
                # Value wasn't specified so fill in zeros
                val = [0] * array_len

            #if self._verbose: print " Field value:",val
                
            # Add the value(s)
            buffer += struct.pack(fmt_string,*val)

        if self._verbose:
            print 'payload=[',
            for b in buffer:
                print hex(ord(b)),
            print ']'


        # Store the raw payload string back into the packet object and return
        pkt["payloadraw"]=buffer
            
        return pkt

    def pretty_print_packet(self,pkt):
        """ This function results the packet as string. """

        try:
            s = pkt['name'] + ' ['
        except (TypeError,KeyError):
            s= "UNKNOWN" + ' ['
            return s
            
        try:
            for f in pkt['fields']:
                fn = f['name']

                try:
                    s+= "%s=%s "%(fn, str(pkt['values'][fn]))
                except KeyError: # Catch cases where value does not exist
                    s+= "%s=DNE! "%(fn)
        except KeyError:
            s+="ERROR: packet is lacking 'fields' key"

        s += "]"


        return s

if __name__ == "__main__":
    import argparse
    import framer
    import serial
    import sys
    import json

    # Define parser for command line arguments
    ap = argparse.ArgumentParser(description='Packet builder script.')
    ap.add_argument('-v','--verbose',action='count',default=0,
                        dest='verbosity',help='increase output vebosity')
    ap.add_argument("port",help='serial port (e.g. /dev/ttyUSB0 or COM1)')
    ap.add_argument("json",help='JSON description of packet')

    # Parse the command line arguments
    args = ap.parse_args()

    # connect to serial port
    ser = serial.Serial()
    ser.baudrate = 115200
    ser.port     = args.port
    ser.timeout  = 1     # required so that the reader thread can exit
    ser.open()
    
    # Create framer and packet builder objects
    pktf = framer.BinaryPacketTransmitter(ser)
    pktb = Builder()

    # Load JSON packet from file
    with open(args.json,'r') as f:
        pkt = json.load(f)

    # TODO: Prompt user for fields

    # Construct the packet
    pkt_raw_bytes = pktb.build_packet(pkt)

    # Send it
    pktf.xmit_packet(pkt_raw_bytes)

    print "closing serial port"
    ser.close()
