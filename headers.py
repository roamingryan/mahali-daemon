####################################################################
# Generate C Headers from Packet Definitions
#
# author: Ryan Kingsbury
#
# This module can be used to generate C header files from the
# specified packet definitions.  This module also checks the syntax of
# the definition header files.
#
# TODO: Add check for duplicate field names
#
####################################################################
import json
import os
import sys
import struct

# Get data type mappings
import mapping
DATA_TYPE_FMT = mapping.get_mappings()

def gen_struct(j,indent=4):
    """ Generates C struct for packet in specified JSON object.  Returns
    result as string. """

    # OS independent line separator
    nl = os.linesep

    s = ''

    s += "// " + j['description'] + nl
    s += "typedef struct {" + nl

    for field in j['fields']:

        s += ' '*indent*2             # indent
        s += "%-10s"%field['type'] + ' ' + field['name']  # data type and name
        if "array" in field:
            try:
                array_len = int(field['array'])
                s += '[%i]'%array_len
            except ValueError:
                print "Array parameter for field %s is not an integer."
        s += ';'
        try:
            s += ' '*indent + '// ' + field['description'] + nl # description/units
        except KeyError:
            # No description was provided
            s += nl

    s += '} __attribute__ ((packed)) ' + j['C_struct_name'] +';'

    return s

def gen_struct_init(j,indent=4):
    """ Generates C struct definition with initialization based on default values for
    packet in specified JSON object.  Returns result as string. """

    # OS independent line separator
    nl = os.linesep

    s = ''
    s += '// Example definition with defaults initialized' + nl
    s += j['C_struct_name'] + " CHANGE_MY_NAME = {" + nl

    foundDefault = False

    for field in j['fields']:
        # Skip fields without default values
        if 'default' not in field:
            continue
            
        foundDefault = True
            
        s += ' '*indent*2             # indent
        if 'array' in field:
            defaultval = '{' + ((str(field['default'])+',')*field['array'])[:-1] + '},'
        else:
            defaultval = "%-15s"%(str(field['default'])+',')
            
        s += ".%-20s"%field['name'] + ' = ' + defaultval  # data type and name
            
        try:
            s += ' '*indent + '// ' + field['description'] + nl # description/units
        except KeyError:
            # No description was provided
            s += nl

    s += '};'

    if foundDefault:
        return s
    else:
        return ''

def gen_enum(j,indent=4):
    """Generates C enum for the specified field.  Returns result as
    string.
    """

    # OS independent line separator
    nl = os.linesep

    s = ''

    s += "// Enum for field " + j['name'] + nl
    s += "enum {" + nl

    e = j['enum']
    # Make list of (value,name) pairs that we can then sort.
    elist = []
    for val in e:
        elist.append( (int(val),e[val]) )
    elist.sort()
    for (val,name) in elist:
        unique_name = j['name']+'_'+name
        s += ' '*indent*2             # indent
        s += "%-20s"%unique_name.upper() + ' = ' + str(val) + ',' + nl # data type and name

    s += '};'

    return s

def gen_packet_type(j,indent=4):
    """ Generates packet type #define for packet in specified JSON
    object.  Returns result as string. """

    # OS independent line separator
    nl = os.linesep

    s = ''

    defname = "PKT_TYPE_%s"%(j['name'].upper())
    s += "#define %-30s %6i"%(defname,j['id'])
    s += nl

    return s

def calc_pkt_size(j):
    """ Calculates the appropriate length field for the specified
    packet.

    NOTE: The length field excludes the bytes in the header."""

    l=0
    
    for field in j['fields']:

        # Translate the C data type into something the python struct
        # module understands.
        try:
            fmt = DATA_TYPE_FMT[field['type']]
        except KeyError:
            print "ERROR: Data type (%s) was not understood."%str(field['type'])
            
        # See if this field is an array
        if 'array' in field:
            try:
                array_len = int(field['array'])
                fmt = fmt*array_len
            except ValueError:
                print "Array parameter for field %s is not an integer."

        # Calculate the size of the array, add to counter
        l += struct.calcsize(fmt)

    return l
    

if __name__ == "__main__":

    summary = {}
    
    for fname in sys.argv[1:]:
    
        with open(fname,'r') as f:
            j = json.load(f)

            # Skip files that aren't long packet format
            if j['format'].upper() != "LONG":
                sys.stderr.write("Skipping %s because it isn't a long packet type"%fname)
                continue

            summary.setdefault(int(j['id']),[]).append(j['name'])
                
            print "\n-------------------------------------------------------------------"
            print "Generated with headers.py from %s"%fname

            print "Packet length is %i"%calc_pkt_size(j),

            try:
                if calc_pkt_size(j)!=j['length']:
                    print "--> NOTE: DOES NOT MATCH VALUE IN PACKET DEFINITION"
                else:
                    print
            except KeyError:
                print ", this file does not have a length field"

            print ""
            
            print "// Packet type define (should be placed in packets.h)"
            print gen_packet_type(j)

            # Generate and print the structure
            print "// Struct declaration: (normally stored in driver header file)"
            print gen_struct(j)
            print ""
            print gen_struct_init(j)

            print os.linesep

            # Generate enums if there are any
            for f in j["fields"]:
                if "enum" in f:
                    print gen_enum(f) + os.linesep


    # If multiple JSON files were specified, print out a summary of packet IDs
    if len(summary)>1:
        # print summary of packets
        print "Packet definition summary:"
        print "ID\tNAME"
        for id in sorted(summary.keys()):
            print "%i\t%s"% (id, str(summary[id])),
            if len(summary[id])>1:
                print "\t\t<-- CONFLICT!"
            else:
                print

