Dashboard provides a web-based interface for embedded systems.
Telemetry streaming streaming in from the system is logged to disk.
Simultaneously, the user can display and plot the telemetry values in
realtime through the web interface.  Additionally, the user can
construct commands that are sent to the system.  These too are logged
to disk.

Implementation Overview
=======================
`dashboard.py` makes use of the Twisted (http://twistedmatrix.com)
asynchronous programming framework.  The web interface is provided by
the Flask microframework (http://flask.pocoo.org/).  The Flask web
interface is served up using Twisted WSGI (HTTP) server bindings.
Bootstrap CSS (http://getbootstrap.com/2.3.2/) is used to make the
whole thing pretty.

Dependencies and Installation
=============================

It is highly recommended that users install `virtualenv` on their
system.  This allows one to create a self-contained Python environment
with the specific dependencies needed for this project.  Once
`virtualenv` has been installed, the virtual environment can be
populated with the necessary python dependencies using pip and the
provided `requirements.txt` file.

    pip install -r requirements.txt

Summary of dependencies:
    * Python 2.7 or later (untested with Python 3.x!)
    * Twisted
    * Flask
    * pyserial (http://pyserial.sourceforge.net/)
    * numpy and scipy (necessary for MATLAB output only)


File Descriptions
=================

`dashboard.py` is the main script.  For invocation help:

    python dashboard.py --help

`requirements.txt` contains a list of Python packages that are
required by this application.  It can be used in conjunctoin with the
popular `pip install` tool to setup an appropriate Python environment
with the libraries needed for Dashboard.  More info is below in the
installation section.

`./packet_defs/` contains JSON-formatted packet definition files.
These files describe each of the expected packet types include: unique
TYPE, length and a list of fields contained in the payload of the
packet.  Each field description includes: data type (e.g. uint16_t),
name, description.  Optionally, field descriptions can also contain an
"enum" declaration, or a formatting function that is applied to the
data in the field prior to display.  Additional information can be
found below.

`deframer.py` contains the state machine that unpacks packets from the
  incoming byte stream.  This process involves reversing the
  byte-stuffing process, checking length fields, CRCs, etc.  Custom
  framing protocols can be added as desired by modifying this file.

`framer.py` performs the reverse process of the `deframer.py`

`pktparser.py` takes deframed packets from deframer.py and attempts
  to parse them using the packet formats specified in the
  ./packet_defs/ JSON files.  The packet's TYPE field is used to
  match an incoming packet with the appropriate definition.

`pktbuilder.py` performs the reverse process of `pktparser.py`

`mapping.py` contains a dictionary that maps the data type specifiers
  used in the JSON packet definition files to the to the appropriate
  formatting string that can be consumed by the Python `struct` module.
  These mostly match the widely-accepted definitions found in the ANSI
  stdint.h header file.

`headers.py` can be used to automatically generate C headers and
  #defines from the JSON packet definitions.  This script can also be
  used to validate the formatting of the JSON packet definitions.

`hardware_sim.py` emulates the embedded system by generating packets on
  a TCP port.  This is useful for development when the hardware is not
  available. 

`/webif/` the Flask web application

`/webif/__init__.py` contains Flask internal settings

`/webif/views.py` contains web "views" which essentially assign a
Python function to individual URLs

`/webif/templates` contains HTML templates 

`/webif/static` static files are served from this directory.  This is
where custom JavaScript files, images, etc should be installed.

Packet/Message Definition File Format
=====================================

Packet definition files should act as the primary reference for packet formats.
They are the law.  Both packet parsing and formation in Dashboard are
driven by the contents of these files.  The web interface (e.g. the
command input forms) are generated dynamically from these files.
Additionally, C headers which can be used directly in the embedded
system can be generated from the packet definition files using the 
`headers.py` script.

Each packet definition file contains a list of all the fields
contained within a given packet type.  The definition file also
provides additional metadata about the packet that is used in the web
interface and code generation scripts.

Message Parameters
------------------
    - `id` is the unique packet ID for this packet (integer 0-255, not 0x30)
    - `C_struct_name` is string used in headers.py for C code generation
    - `framing` parameter specifies how the packet is encapsulated for transmission
      this field is ignored for Serial (USB) link types.  For
      specialty links where a mix of framing methods are used, this
      field can be used to pair a given message type with a particular
      framing scheme. 

Field Parameters
----------------
Each field may carry one or more of the designators below.

    * `default`
    * `array`
    * `type`
    * `name`
    * `description`
    * `enum`


History of Dasboard
===================
The Dashboard tool was originally developed in the MIT Space Systems
Laboratory in support of the Micro-sized Microwave Atmospheric
Satellite (MicroMAS) program, a student-built 3U CubeSat.  Although
the tool was originally created as a tool to help with debugging of
the satellite hardware, it can be applied to any type of embedded
system.




