####################################################################
# Generate MATLAB structure from binary packet logs
#
# author: Ryan Kingsbury
#
# This script parses binary packet logs (base64 encoded) into MATLAB
# structures.
#
# For command line help:
#     python gen_matlab.py --help
#
####################################################################

# External libraries
from scipy import io

# Custom libraries/modules
import pktlog
import pktparser

def convert_to_matlab(input_filename, output_filename=None, compress=True, append_rx_time=False, use_enum=False, pkt_filter=None):
    """ Converts a the specified binary packet log into a .mat file for MATLAB.
    """
    
    # Create dictionary where we can store all parsed packets
    pdb = {}

    parser = pktparser.Parser()
    
    with open(input_filename,"r") as fin:
        cnt = 0
        for line in fin:
            (time,pktraw,framingType) = pktlog.read_entry(line)

            if framingType != "OBC":
                continue

            cnt += 1

            pkt = parser.parse_packet(pktraw, use_enum=use_enum)
            
            if pkt==None:
                # Parser failure
                print "Failed to parse:", line
                continue

            # If packet filters have been specified, observe them
            if pkt_filter:
                if pkt['name'] not in pkt_filter:
                    continue

            pkt['values']['logfile_line_num'] = cnt

            if append_rx_time:
                pkt['values']['pkt_rx_utc']=pktlog.datetime_to_iso8601(time) 
                
            # Add packet to database
            pdb.setdefault(pkt['name'], []).append(pkt['values'])

    print "Read %i packets"%cnt
    
    # write to disk
    if output_filename==None:
        output_filename = input_filename + ".mat"

    print "saving matlab file to",output_filename
    io.savemat(output_filename,pdb,oned_as='row',do_compression=compress)

    return
    
if __name__ == '__main__':
    import argparse

    # Define parser for command line arguments
    ap = argparse.ArgumentParser(description='gen_matlab.py')
    ap.add_argument("file", help='input file', nargs='+')
    ap.add_argument('-c','--compress',
                    action='store_true',
                    dest='compress',
                    help='enable MAT file compression (CPU intensive)')
    ap.add_argument('--append-time',
                    action='store_true',
                    dest='appendTime',
                    help='appends the time (UTC) the packet was received at the ground station to each packet')
    ap.add_argument('-e','--use-enum',
                    action='store_true',
                    dest='useEnum',
                    help='observe enum mappings specified in packet definition')
    ap.add_argument('-f','--filter',
                    dest='pfilter',
                    default=None,
                    nargs='+',
                    help='filter packet(s) by type (e.g. TLM_TEST), multiple types can be specified')

    
    # Parse the command line arguments
    args = ap.parse_args()

    for f in args.file:
        print "Processing",f
        convert_to_matlab(f,
                          compress=args.compress,
                          append_rx_time=args.appendTime,
                          use_enum=args.useEnum,
                          pkt_filter=args.pfilter)

        
