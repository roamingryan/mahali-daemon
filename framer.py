####################################################################
# Packet Framers
#
# author: Ryan Kingsbury
#
# This module is used to frame packets for the specified link mode.
# Framers are responsible for adding the appropriate packet header
# and any sort of special packet delimiting (e.g. byte stuffing).
#
# A single "byte-stuffed" framer is provided below.
#
####################################################################
import struct
import crc16pure as crc16

SERIAL_FRAME_ESCAPE_FLAG = 0x30

class BaseFramer(object):
    """Common framer methods and objects."""

    def __init__(self,output_fd):
        """Initialize binary packet transmitter object.

        A serial port (or similar file object featuring a write())
        function must be provided.
        """
        
        self.stats = {"BYTES_SENT":0,
                      "PACKETS_SENT":0,
                      "LARGEST_PACKET_SENT":0,
                      }

        self._output_fd = output_fd

        # MicroMAS command sequence number counter,
        # will wrap to zero on first command transmission
        self.stats["PREV_CMD_SEQ_ID_COUNTER"] = 2**16-1

    def get_next_seq_id(self):
        """ Creates a new command sequence ID

        This command sequence ID is checked by the flight software.
        Sequence IDs must be monotonically increasing in order for the
        command to be acted upon by the mission manager.
        """
        self.stats["PREV_CMD_SEQ_ID_COUNTER"] += 1
        self.stats["PREV_CMD_SEQ_ID_COUNTER"] %= 2**16 # wrap around (16-bit field)
        return self.stats["PREV_CMD_SEQ_ID_COUNTER"]

    def get_seq_id(self):
        """ Gets what will be the next sequence ID without incrementing
        """
        return (self.stats["PREV_CMD_SEQ_ID_COUNTER"]+1)%(2**16)

    def set_next_seq_id(self,newVal):
        """ Sets the next sequence ID

        This function is used to set the framer's internal sequence
        counter to the desired value.  This is needed when the
        Dashboard first starts up as it is out of sequence with the
        satellite.'"""

        self.stats["PREV_CMD_SEQ_ID_COUNTER"] = (newVal-1)%(2**16)

        return
        
class SerialFramer(BaseFramer):
    """Byte-stuffing based framer for use with serial and USB links."""
        
    def __init__(self,output_fd,verbose=True,escape_flag=SERIAL_FRAME_ESCAPE_FLAG):
        BaseFramer.__init__(self,output_fd)

        # Define escape flag
        self._ESCAPE_FLAG = escape_flag       # Marks SOP

        # Add statistics fields specific to the SerialFramer
        self.stats["ERR_PKT_SOP_NOT_ESCAPE"] = 0  # the packet SOP byte must be the escape character
        self.stats["ERR_PKT_ID_IS_ESCAPE"] = 0 # the packet ID must not be the escape character
        self.stats["BYTES_STUFFED"] = 0
        self.stats["ERR_PKT_FRAMING_TYPE_INVALID"] = 0

        self._verbose = verbose
        
    def xmit_packet(self,pkt):
        """ Transmit the specified string of bytes."""

        # Check for framing specification, if it is something other than OBC, throw it out
        pktframing = pkt.get('framing',"OBC").upper()
        if pktframing.upper() != "OBC":
            print "Warning: user attempted to send Cadet/CC1101 packet while in serial mode...framer is discarding"
            self.stats["ERR_PKT_FRAMING_TYPE_INVALID"] += 1
            return

        if self._verbose: print "Appending frame header for",pkt['name'],
        
        # Temporary variable where we will construct the packet
        buffer = ""

        # Add start of packet indicator
        buffer += struct.pack("B",self._ESCAPE_FLAG)

        # Add packet ID
        buffer += struct.pack("B",pkt['id'])
        
        # Short packets can be transmitted immediately since they
        # don't require stuffing or CRC calculation. 
        if pkt['format'] == 'short':
            if self._verbose:
                print '[',
                for b in tx_buffer:
                    print hex(ord(b)),
                print ']'
            self._output_fd.write(buffer)
            self.stats['BYTES_SENT']+=2
            self.stats['PACKETS_SENT']+=1
            return

        # Add packet length field, TWICE
        # Also add 2 for [SEQ ID] field for bus-bound packets
        if 'hasSequence' in pkt:
            buffer += 2*(struct.pack("<H",len(pkt['payloadraw'])+2))
        else:
            # Payload packets do not include an additional SEQID field
            buffer += 2*(struct.pack("<H",len(pkt['payloadraw'])))

        # Add CRC
        buffer += "\x00\x00"

        # Add sequence ID
        if 'hasSequence' in pkt:
            buffer += struct.pack("<H", self.get_next_seq_id())

        # Add the payload    
        buffer += pkt["payloadraw"]
        
        # calc CRC
        buffer = buffer[0:6] + struct.pack("<H",crc16.crc16xmodem(buffer[8:])) + buffer[8:]

        # Sanity check the packet's SOP and ID fields
        if ( ord(buffer[0]) != self._ESCAPE_FLAG ):
            self.stats['ERR_PKT_SOP_NOT_ESCAPE']+=1
            raise ValueError
        if ( ord(buffer[1]) == self._ESCAPE_FLAG ):
            self.stats['ERR_PKT_ID_IS_ESCAPE']+=1
            raise ValueError

        # Store the bytes that are about to be sent
        pkt['framer_output_bytes'] = buffer
            
        # Transmit the first byte
        tx_buffer = buffer[0]
        self.stats['BYTES_SENT']+=1

        # Transmit the remainder of the bytes in the packet, stuffing
        # as necessary
        for b in buffer[1:]:
            
            # Add byte to transmit buffer
            tx_buffer += b
            self.stats['BYTES_SENT']+=1

            # If it was the ESCAPE char, then send it again
            if (ord(b) == self._ESCAPE_FLAG):
                tx_buffer += b
                self.stats['BYTES_SENT']+=1
                self.stats['BYTES_STUFFED']+=1

            
        # Done sending the packet, update stats
        self.stats['PACKETS_SENT']+=1
        if len(tx_buffer)>self.stats["LARGEST_PACKET_SENT"]:
            self.stats["LARGEST_PACKET_SENT"]=len(tx_buffer)

        # Write the buffer to the output port
        self._output_fd.write(tx_buffer)

        if self._verbose:
            print '[',
            for b in tx_buffer[:8]:
                print hex(ord(b)),
            print ']+[%i payload bytes], transmitted'%(len(tx_buffer)-8)
            
        return pkt

